package behappy.edu.Posts;

import javax.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
    public class Post {
        @Id
        @GeneratedValue(strategy= GenerationType.AUTO)
        @Column(name="id")
        private Long id;

        @Column(name="author")
        private String author;

        @Column(name="content")
        private String content;


        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }


    }



