package behappy.edu.Posts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;


@Controller
@RequestMapping(path="/post")
public class MainController {
	@Autowired
	private PostRepository postRepository;
	
	@GetMapping(path="/add")
	public @ResponseBody String addNewPost (@RequestParam String author
			, @RequestParam String content) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		
		Post newPost = new Post();
		newPost.setAuthor(author);
		newPost.setContent(content);
		postRepository.save(newPost);
		return "Dodales zajebisty wpis "+author+" !!";
	}
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<Post> getAllUsers() {
		return postRepository.findAll();
	}

	@RequestMapping(path="/{n}", method = RequestMethod.GET)
	public @ResponseBody
	Post getOneUser(@PathVariable("n") Long id){
        Post fPost = new Post();
		fPost = postRepository.findOne(id);
		return fPost;
	}

	@RequestMapping(path ="/{n}/del")
    public @ResponseBody
    String delOneUser(@PathVariable("n") Long id){
	    Post fPost = postRepository.findOne(id);
	    String name=fPost.getAuthor();

	    postRepository.delete(id);
	    return "Wpis o nr: "+id.toString()+" autora: "+name+" zostal usuniety!";
    }
}
