package behappy.edu.Posts;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sun.applet.Main;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PostsApplicationTests {

	@Autowired MainController mainController;

	@Test
	public void contextLoads() {
		assertThat(mainController).isNotNull();
	}

}
